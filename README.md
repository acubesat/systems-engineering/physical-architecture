# Physical Architecture

## Introduction

This repository includes block diagrams of the physical arcitecture for the AcubeSAT nanosatellite. Readers can see the diferent subsystems breakdown into hardware products/elements and how everything is interfaces and connects to the different elements

All diagrams have been created using the [draw.io](https://app.diagrams.net/) diagram software. The most up-to-date diagram can be seen below:

## Current AcubeSAT Physical Architecture

![](./AcubeSAT_Physical_Architecture.svg)

## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.