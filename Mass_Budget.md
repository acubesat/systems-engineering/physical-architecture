| Component                               | Mass (g) | Margin (%) | Mass with margin (g) |
|-----------------------------------------|----------|------------|----------------------|
| Comms Board                             | 101,4    | 20         | 121,68               |
| Nanoavionics EPS + 2S2P Battery         | 329,6    | 5          | 346,08               |
| PC/104 Connector for OBDH/ADCS Board    | 16,4     | 5          | 17,22                |
| CubeSat Frame                           | 280,89   | 5          | 294,93               |
| OBDH/ADCS Assembled PCB                 | 57       | 20         | 68,4                 |
| PC/104 Spacers for all SS Boards        | 55,54    | 10         | 61,094               |
| DIN 965 Philips Screws (x72)            | 72       | 5          | 75,6                 |
| Payload Container                       | 916,96   | 20         | 1100,352             |
| Payload Container Support Rings         | 40,4     | 20         | 48,48                |
| Payload Unibody                         | 305,32   | 20         | 366,38               |
| Camera Lens                             | 69       | 5          | 72,45                |
| Camera Sensor                           | 26       | 5          | 27,3                 |
| SU Chip Glass                           | 6,25     | 20         | 7,5                  |
| SU Medium Container                     | 50       | 20         | 60                   |
| Microfluidic Chip                       | 3,7      | 20         | 4,44                 |
| SU Pressure Sensor #1                   | 6,59     | 5          | 6,9195               |
| SU Pressure Sensor #2                   | 6,59     | 5          | 6,9195               |
| SU valves mounting space                | 20       | 20         | 24                   |
| SU PCB                                  | 40       | 20         | 48                   |
| SU contol pump                          | 42       | 10         | 46,2                 |
| SU flow pump                            | 42       | 10         | 46,2                 |
| AD590 Temperature Sensor 1              | 1        | 5          | 1,05                 |
| AD590 Temperature Sensor 2              | 1        | 5          | 1,05                 |
| AD590 Temperature Sensor 3              | 1        | 5          | 1,05                 |
| SU Latching Solenoid Valve (x8)         | 20       | 5          | 21                   |
| SU Control Solenoid Valves (x6)         | 27       | 5          | 28,35                |
| SU Excitation LED Board                 | 1,86215  | 20         | 2,23458              |
| SU LED (x4)                             | 0,76078  | 5          | 0,798819             |
| SU Heater 1                             | 2,2679   | 5          | 2,381295             |
| SU Water Container                      | 50       | 20         | 60                   |
| Assembled Deployable Antenna            | 141,102  | 20         | 169,3224             |
| Antennas' coaxial cables and connectors | 30       | 20         | 36                   |
| ISIS Magnetorquer Board                 | 196      | 5          | 205,8                |
| Payload Antenna                         | 25       | 20         | 30                   |
| RW210 Hyperion Reaction Wheel           | 21       | 5          | 22,05                |
| Solar Panel -Y                          | 136      | 5          | 142,8                |
| Solar Panel +Y                          | 136      | 5          | 142,8                |
| Solar Panel -X                          | 136      | 5          | 142,8                |
| Solar Panel +X                          | 136      | 10         | 149,6                |
| Solar Panel Z                           | 48       | 5          | 50,4                 |
| Vacuum Feedthrough                      | 23,83    | 20         | 28,596               |
| Spacecraft Wiring                       | 12,249   | 20         | 14,699               |
| **TOTAL (g)**                           | **3613,71** | **TOTAL (with margin, g)** | **4078,94**|
| |System-wide margin|5%| |
| **TOTAL (g) + system margin**           | **3794,40** | **TOTAL (with margin, g) + system margin** | **4259,62**|
